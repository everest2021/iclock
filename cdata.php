<?php
// https://gist.github.com/jva6438/f3a1f1f1bbee52a933c8233246328067
// forked from https://gist.github.com/magnetikonline/650e30e485c0f91f2f40

error_reporting(0);

error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Report all errors
error_reporting(E_ALL);

// Same as error_reporting(E_ALL);
ini_set("error_reporting", E_ALL);

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);
require_once('config.php');
//echo 'inn';

class DumpHTTPRequestToFile {
	
	public function execute($targetFile) {
		//echo '</br>';
		//echo 'innnnnn';
		//exit;\n
		//$data = "innn\n";
		$data = sprintf("%s %s %s %s\n\n## HTTP headers:\n",
            date("Y-m-d H:i:s"),
			$_SERVER['REQUEST_METHOD'],
			$_SERVER['REQUEST_URI'],
			$_SERVER['SERVER_PROTOCOL']
		);
		foreach ($this->getHeaderList() as $name => $value) {
			$data .= $name . ': ' . $value . "\n";
		}
        
		$this->conn = new mysqli(HOSTNAME, USERNAME, PASSWORD, DATABASE);
	
		if ($this->conn->connect_error) {
			fwrite($handle, date('Y-m-d G:i:s') . '-' . print_r($this->conn->connect_error, true)  . "\n");
			die("Connection failed: " . $this->conn->connect_error);
		}
		
		
        $data .= "\n\r## Request body:\n" . file_get_contents('php://input') . "\n";
        $data .= "\nGET:\n" . print_r($_GET, TRUE);
        $data .= "\nPOST:\n" . print_r($_POST, TRUE);

        $attend_data = file_get_contents('php://input');
        $get_datas = explode("\n", $attend_data);
        foreach ($get_datas as $gkey => $gvalue) {
			if($gvalue){
				
				$user_id = '';
		        $all_punch_datas = explode("\t", $gvalue);
				$emp_id = $all_punch_datas[0];
				$data .= "\n foreach datas: \n" . print_r($emp_id, TRUE );

				if(isset($emp_id) && $emp_id != ""){
					if(strpos($emp_id, 'OPLOG') !== false ){
						$oplog = $all_punch_datas[0].''.$all_punch_datas[1];
						$insert = $this->query("INSERT INTO `oc_oplog` SET 
								oplog = '".$oplog."',
								date_time = '".$$all_punch_datas[2]."',
								c1 = '".$all_punch_datas[3]."',
								c2 = '".$all_punch_datas[4]."',
								c3 = '".$all_punch_datas[5]."',
								c4 = '".$all_punch_datas[6]."',
								c5 = '".$c5."' ",$this->conn);

					} elseif(strpos($emp_id, 'USER') !== false ) {

				      	$user_id = explode('=', $all_punch_datas[0]);
				      	$emp_name = explode('=', $all_punch_datas[1]);
				      	$priority = explode('=', $all_punch_datas[2]);
				      	$password = explode('=', $all_punch_datas[3]);
				      	$card = explode('=', $all_punch_datas[4]);
				      	$status = explode('=', $all_punch_datas[5]);
				      	$tz = explode('=', $all_punch_datas[6]);
							
				      	$emp_master_name = ($emp_name[1] != "") ?  $emp_name[1]: $user_id[1] ;
						
						$last_employee = $this->query("SELECT * FROM `oc_employee` WHERE device_emp_code = '".$user_id[1]."' ", $this->conn);
						if($last_employee->num_rows == 0) {
							$insert = $this->query("INSERT INTO `oc_employee` SET 
									device_emp_code = '".$this->escape($user_id[1], $this->conn)."',
									emp_name = '".$this->escape($emp_master_name, $this->conn)."',
									priority = '".$this->escape($priority[1], $this->conn)."',
									password = '".$this->escape($password[1], $this->conn)."',
									status = '".$this->escape($status[1], $this->conn)."',
									device_serial_no = '".$this->escape($_GET['SN'], $this->conn)."',
									card = '".$this->escape($card[1], $this->conn)."' ,
									tz = '".$this->escape($tz[1], $this->conn)."' 

									",$this->conn);
						}
				 	} elseif(strpos($emp_id, 'FACE') !== false ) {
						$insert = $this->query("INSERT INTO `oc_emp_bio` SET 
								temp = '".$gvalue."' ",$this->conn);
					} elseif(strpos($emp_id, 'FP') !== false ) {
						$insert = $this->query("INSERT INTO `oc_emp_bio` SET 
								temp = '".$gvalue."' ",$this->conn);
				 	} else {
						$date_time = explode(' ', $all_punch_datas[1]);
						$date = $date_time[0];
						$time = $date_time[1];
						$device_id = $all_punch_datas[2];
						$c_id = $all_punch_datas[3];
				
						$pre_data = $this->query("SELECT * FROM `oc_attendance` WHERE emp_id = '".$emp_id."' AND punch_date = '".$date."' AND punch_time = '".$time."' ",$this->conn);
						if($pre_data->num_rows == 0) {
							$insert = $this->query("INSERT INTO `oc_attendance` SET 
								emp_id = '".$emp_id."',
								punch_date = '".$date."',
								punch_time = '".$time."',
								device_id = '".$this->escape($_GET['SN'], $this->conn)."'
								",$this->conn);
				 		}
				 	}
				}
		 	}
		}
    


		file_put_contents(
			$targetFile,
			$data . "\n",
            FILE_APPEND
		);
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		     // The request is using the POST method
			header("Content-Type: text/plain");
			echo ("OK \n");

		}else if($_SERVER['REQUEST_METHOD'] === 'GET'){
			"GET:" . print_r($_GET, TRUE);
			header("Content-Type: text/plain");
			echo "Stamp=0 \n";
			echo "OpStamp=0 \n";
			echo "PhotoStamp=9999 \n";
			echo "ErrorDelay=60 \n";
			echo "Delay=30 \n";
			echo "TransTimes=18:20;18:25 \n";
			echo "TransInterval=1 \n";
			echo "TransFlag=111111101101 \n";
			echo "Realtime=1 \n";
			echo "TimeOut=60 \n";
			echo "TimeZone=330 \n";
			echo "Encrypt=0 \n";
		}
	}
	private function getHeaderList() {
		$headerList = [];
		foreach ($_SERVER as $name => $value) {
			if (preg_match('/^HTTP_/',$name)) {
				// convert HTTP_HEADER_NAME to Header-Name
				$name = strtr(substr($name,5),'_',' ');
				$name = ucwords(strtolower($name));
				$name = strtr($name,' ','-');
				// add to list
				$headerList[$name] = $value;
			}
		}
		return $headerList;
	}

	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}

	public function query($sql, $conn) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}
}
(new DumpHTTPRequestToFile)->execute('./cdata.txt');