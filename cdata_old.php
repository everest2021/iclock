<?php
// https://gist.github.com/magnetikonline/650e30e485c0f91f2f40
class DumpHTTPRequestToFile {
	

	public function execute($targetFile) {
		$data = sprintf(
			"%s %s %s\n\nHTTP headers:\n",
			$_SERVER['REQUEST_METHOD'],
			$_SERVER['REQUEST_URI'],
			$_SERVER['SERVER_PROTOCOL']
		);
		foreach ($this->getHeaderList() as $name => $value) {
			$data .= $name . ': ' . $value . "\n\r";
		}
		$data .= "\nRequest body:\n";
		file_put_contents(
			$targetFile,
			$data .= file_get_contents('php://input') . "\n\r"
			 , FILE_APPEND | LOCK_EX
			
		);
                echo 'Stamp=0<br> 
                OpStamp=9999 <br>
                PhotoStamp=9999 <br>
                ErrorDelay=600 <br>
                Delay=600 <br>
                TransTimes=18:20;18:25 <br>
                TransInterval=10 <br>
                TransFlag=111111101101 <br>
                Realtime=1 <br>
                TimeOut=60 <br>
                TimeZone=330 <br>
                Encrypt=0';
	}
	private function getHeaderList() {
		$headerList = [];
		foreach ($_SERVER as $name => $value) {
			if (preg_match('/^HTTP_/',$name)) {
				// convert HTTP_HEADER_NAME to Header-Name
				$name = strtr(substr($name,5),'_',' ');
				$name = ucwords(strtolower($name));
				$name = strtr($name,' ','-');
				// add to list
				$headerList[$name] = $value;
			}
		}
		return $headerList;
	}
}
(new DumpHTTPRequestToFile)->execute('./cdata.txt');