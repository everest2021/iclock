<?php
// https://gist.github.com/magnetikonline/650e30e485c0f91f2f40
class DumpHTTPRequestToFile {
	

	public function execute($targetFile) {
		$data = sprintf(
			"%s %s %s\n\nHTTP headers:\n",
			$_SERVER['REQUEST_METHOD'],
			$_SERVER['REQUEST_URI'],
			$_SERVER['SERVER_PROTOCOL']
		);
		foreach ($this->getHeaderList() as $name => $value) {
			$data .= $name . ': ' . $value . "\n\r";
		}
		$data .= "\nRequest body:\n";
		file_put_contents(
			$targetFile,
			$data .= file_get_contents('php://input') . "\n\r"
			 , FILE_APPEND | LOCK_EX
			
		);
                
header("Content-Type: text/plain");
echo "OK \n";
//echo "OpStamp=9999 \n";
//echo "PhotoStamp=9999 \n";
//echo "ErrorDelay=60 \n";
//echo "Delay=30 \n";
//echo "TransTimes=18:20;18:25 \n";
//echo "TransInterval=1 \n";
//echo "TransFlag=111111101101 \n";
//echo "Realtime=1 \n";
//echo "TimeOut=60 \n";
//echo "TimeZone=330 \n";
//echo "Encrypt=0 \n";

	}
	private function getHeaderList() {
		$headerList = [];
		foreach ($_SERVER as $name => $value) {
			if (preg_match('/^HTTP_/',$name)) {
				// convert HTTP_HEADER_NAME to Header-Name
				$name = strtr(substr($name,10),'_',' ');
				$name = ucwords(strtolower($name));
				$name = strtr($name,' ','-');
				// add to list
				$headerList[$name] = $value;
			}
		}
		return $headerList;
	}
}
(new DumpHTTPRequestToFile)->execute('./devicecmd.txt');