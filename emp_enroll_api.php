<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors",1);
require_once('config.php');
$file = 'C:/xampp/htdocs/machine_api/service.txt';
$handle = fopen($file, 'a+'); 

$Userapi = new Userapi();
$value = $Userapi->getuserdata($_GET, $handle);
fclose($handle);
exit(json_encode($value));
class Userapi {
	public $conn;
	public function __construct() {
		// $servername ="103.87.174.206";
		// $connectionInfo = array("Database"=>"SmartOfficedb", "UID"=>"sa", "PWD"=>"gymtime@123");
		// //$connectionInfo = array("Database"=>"eBioserver");
		// $this->conn = sqlsrv_connect($servername, $connectionInfo);
		// if($this->conn) {
		// 	//echo "Connection established.<br />";exit;
		// } else {
		// 	//echo "Connection could not be established.<br />";
		// 	//echo '<pre>';
		// 	//print_r(sqlsrv_errors());
		// 	//exit;
		// }
		
		$this->conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		
	}

	public function getLastId($conn){
		return $conn->insert_id;
	}

	public function query_mssql($sql, $conn, $handle) {
		$stmt2 = sqlsrv_query($conn, $sql);
		if($stmt2 === false) {
				//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r('Reset query error', true)  . "\n");
				// $dat['query_error'] = json_encode(sqlsrv_errors());
				// echo'<pre>';
				// print_r(sqlsrv_errors());
				// exit;		
		} else {
			//fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r('Reset query success', true)  . "\n");
		}	
	}

	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}

	public function query_mysql($sql, $conn, $handle) {
		$query = $conn->query($sql);

		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getuserdata($data = array(), $handle){
		date_default_timezone_set("Asia/Kolkata");
		// if(!isset($data['emp_code'])){
		// 	$data['emp_code'] = '4444';
		// }
		if(!isset($data['emp_name'])){
			$data['emp_name'] = '4444';
		}
		if(!isset($data['serial_number'])){
			$data['serial_number'] = 'BJ2C185260427';
		}

		if(isset($data['emp_code']) && $data['emp_code'] != ''){
			$emp_code = $data['emp_code'];
		} else {
			$emp_codes = $this->query_mysql("SELECT * FROM `oc_employee` ORDER BY CAST(device_emp_code AS UNSIGNED) DESC LIMIT 1", $this->conn, $handle);
			if($emp_codes->num_rows > 0){
				$emp_code = $emp_codes->row['device_emp_code'] + 1;
			} else {
				$emp_code = '1000';
			}
		}
		$result = array();
		if(isset($data['emp_name'] ) && $data['emp_name'] != ''){
			$this->query_mysql("INSERT INTO oc_employee SET `emp_name` = '" .$this->escape($data['emp_name'] , $this->conn) . "',`device_emp_code` = '" .$this->escape($emp_code , $this->conn) . "', `device_serial_no` = '" .$this->escape($data['serial_number'] , $this->conn) . "',`status` = '1',`send_to_device` = '1' ", $this->conn, $handle);
			$result['user_data']['status'] = 1; 
		} else {
			$result['user_data']['status'] = 0;
		}
		return $result;
	}

	public function utf8_substr($string, $offset, $length = null) {
		if ($length === null) {
			return iconv_substr($string, $offset, utf8_strlen($string), 'UTF-8');
		} else {
			return iconv_substr($string, $offset, $length, 'UTF-8');
		}
	}
}
?>

