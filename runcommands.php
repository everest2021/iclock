<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('config.php');

$Itemapi = new Itemapi();
$value = $Itemapi->getitem();
echo 'done';exit;
exit(json_encode($value));

class Itemapi {
	public $conn;
	
	public function escape($value, $conn) {
		return $conn->real_escape_string($value);
	}
	public function getLastId($conn){
		return $conn->insert_id;
	}
	public function query($sql, $conn) {
		$query = $conn->query($sql);
		if (!$conn->errno){
			if (isset($query->num_rows)) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				unset($data);
				$query->close();
				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
			exit();
		}
	}

	public function getitem(){ 
		$this->conn = new mysqli(HOSTNAME, USERNAME, PASSWORD, DATABASE);
		// Check connection
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$targetFile = 'getrequest.php';
		$fh = fopen( $targetFile, 'w' );
		fclose($fh);

		$sql = $this->query("SELECT * FROM `oc_device_commands` WHERE status = 0 ", $this->conn);
		if($sql->num_rows > 0){
			$data = '<?php '."\n";
			$data .= 'header("Content-Type: text/plain");'."\n";
			foreach ($sql->rows as $rkey => $rvalue) {
				$data .= 'echo " ' . $rvalue['command'] .'" ; ' . "\n";
			}
			$data .= 'echo "OK";'."\n";
		// echo'<pre>';
		// print_r($data);
		// exit;

			file_put_contents(
				$targetFile,
				$data . "\n",
	            FILE_APPEND
			);

			$this->query("UPDATE `oc_device_commands` SET status = 1", $this->conn);
		} else {
			$data = '<?php '."\n";
			$data .= 'header("Content-Type: text/plain");'. "\n" ;
			//$data .= 'echo " " '. "\n";
			$data .= 'echo "OK";';

			file_put_contents(
				$targetFile,
				$data . "\n",
	            FILE_APPEND
			);

		}
	}

}

?>
